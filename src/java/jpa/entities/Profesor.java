/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Windows
 */
@Entity
@Table(name = "profesor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Profesor.findAll", query = "SELECT p FROM Profesor p"),
    @NamedQuery(name = "Profesor.findByIdProfesor", query = "SELECT p FROM Profesor p WHERE p.idProfesor = :idProfesor"),
    @NamedQuery(name = "Profesor.findByNombresP", query = "SELECT p FROM Profesor p WHERE p.nombresP = :nombresP"),
    @NamedQuery(name = "Profesor.findByCursoP", query = "SELECT p FROM Profesor p WHERE p.cursoP = :cursoP"),
    @NamedQuery(name = "Profesor.findByParaleloP", query = "SELECT p FROM Profesor p WHERE p.paraleloP = :paraleloP"),
    @NamedQuery(name = "Profesor.findByCorreoP", query = "SELECT p FROM Profesor p WHERE p.correoP = :correoP")})
public class Profesor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_profesor")
    private Integer idProfesor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nombres_p")
    private String nombresP;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "curso_p")
    private String cursoP;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "paralelo_p")
    private String paraleloP;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "correo_p")
    private String correoP;
    @OneToMany(mappedBy = "idProfesor")
    private Collection<Preguntas> preguntasCollection;

    public Profesor() {
    }

    public Profesor(Integer idProfesor) {
        this.idProfesor = idProfesor;
    }

    public Profesor(Integer idProfesor, String nombresP, String cursoP, String paraleloP, String correoP) {
        this.idProfesor = idProfesor;
        this.nombresP = nombresP;
        this.cursoP = cursoP;
        this.paraleloP = paraleloP;
        this.correoP = correoP;
    }

    public Integer getIdProfesor() {
        return idProfesor;
    }

    public void setIdProfesor(Integer idProfesor) {
        this.idProfesor = idProfesor;
    }

    public String getNombresP() {
        return nombresP;
    }

    public void setNombresP(String nombresP) {
        this.nombresP = nombresP;
    }

    public String getCursoP() {
        return cursoP;
    }

    public void setCursoP(String cursoP) {
        this.cursoP = cursoP;
    }

    public String getParaleloP() {
        return paraleloP;
    }

    public void setParaleloP(String paraleloP) {
        this.paraleloP = paraleloP;
    }

    public String getCorreoP() {
        return correoP;
    }

    public void setCorreoP(String correoP) {
        this.correoP = correoP;
    }

    @XmlTransient
    public Collection<Preguntas> getPreguntasCollection() {
        return preguntasCollection;
    }

    public void setPreguntasCollection(Collection<Preguntas> preguntasCollection) {
        this.preguntasCollection = preguntasCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProfesor != null ? idProfesor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Profesor)) {
            return false;
        }
        Profesor other = (Profesor) object;
        if ((this.idProfesor == null && other.idProfesor != null) || (this.idProfesor != null && !this.idProfesor.equals(other.idProfesor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Profesor[ idProfesor=" + idProfesor + " ]";
    }
    
}
