/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Windows
 */
@Entity
@Table(name = "preguntas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Preguntas.findAll", query = "SELECT p FROM Preguntas p"),
    @NamedQuery(name = "Preguntas.findByIdPreguntas", query = "SELECT p FROM Preguntas p WHERE p.idPreguntas = :idPreguntas"),
    @NamedQuery(name = "Preguntas.findByTituloPreguntas", query = "SELECT p FROM Preguntas p WHERE p.tituloPreguntas = :tituloPreguntas")})
public class Preguntas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_preguntas")
    private Integer idPreguntas;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "titulo_preguntas")
    private String tituloPreguntas;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "enunciado")
    private String enunciado;
    @JoinColumn(name = "id_profesor", referencedColumnName = "id_profesor")
    @ManyToOne
    private Profesor idProfesor;
    @JoinColumn(name = "id_quiz", referencedColumnName = "id_quiz")
    @ManyToOne
    private Quiz idQuiz;
    @JoinColumn(name = "id_respuestas", referencedColumnName = "id_respuestas")
    @ManyToOne
    private Respuestas idRespuestas;

    public Preguntas() {
    }

    public Preguntas(Integer idPreguntas) {
        this.idPreguntas = idPreguntas;
    }

    public Preguntas(Integer idPreguntas, String tituloPreguntas, String enunciado) {
        this.idPreguntas = idPreguntas;
        this.tituloPreguntas = tituloPreguntas;
        this.enunciado = enunciado;
    }

    public Integer getIdPreguntas() {
        return idPreguntas;
    }

    public void setIdPreguntas(Integer idPreguntas) {
        this.idPreguntas = idPreguntas;
    }

    public String getTituloPreguntas() {
        return tituloPreguntas;
    }

    public void setTituloPreguntas(String tituloPreguntas) {
        this.tituloPreguntas = tituloPreguntas;
    }

    public String getEnunciado() {
        return enunciado;
    }

    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }

    public Profesor getIdProfesor() {
        return idProfesor;
    }

    public void setIdProfesor(Profesor idProfesor) {
        this.idProfesor = idProfesor;
    }

    public Quiz getIdQuiz() {
        return idQuiz;
    }

    public void setIdQuiz(Quiz idQuiz) {
        this.idQuiz = idQuiz;
    }

    public Respuestas getIdRespuestas() {
        return idRespuestas;
    }

    public void setIdRespuestas(Respuestas idRespuestas) {
        this.idRespuestas = idRespuestas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPreguntas != null ? idPreguntas.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Preguntas)) {
            return false;
        }
        Preguntas other = (Preguntas) object;
        if ((this.idPreguntas == null && other.idPreguntas != null) || (this.idPreguntas != null && !this.idPreguntas.equals(other.idPreguntas))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Preguntas[ idPreguntas=" + idPreguntas + " ]";
    }
    
}
