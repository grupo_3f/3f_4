/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Windows
 */
@Entity
@Table(name = "estudiantes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Estudiantes.findAll", query = "SELECT e FROM Estudiantes e"),
    @NamedQuery(name = "Estudiantes.findByIdEstudiantes", query = "SELECT e FROM Estudiantes e WHERE e.idEstudiantes = :idEstudiantes"),
    @NamedQuery(name = "Estudiantes.findByNombresE", query = "SELECT e FROM Estudiantes e WHERE e.nombresE = :nombresE"),
    @NamedQuery(name = "Estudiantes.findByCursoE", query = "SELECT e FROM Estudiantes e WHERE e.cursoE = :cursoE"),
    @NamedQuery(name = "Estudiantes.findByParaleloE", query = "SELECT e FROM Estudiantes e WHERE e.paraleloE = :paraleloE"),
    @NamedQuery(name = "Estudiantes.findByCorreoE", query = "SELECT e FROM Estudiantes e WHERE e.correoE = :correoE")})
public class Estudiantes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_estudiantes")
    private Integer idEstudiantes;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nombres_e")
    private String nombresE;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "curso_e")
    private String cursoE;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "paralelo_e")
    private String paraleloE;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "correo_e")
    private String correoE;
    @OneToMany(mappedBy = "idEstudiantes")
    private Collection<Nota> notaCollection;

    public Estudiantes() {
    }

    public Estudiantes(Integer idEstudiantes) {
        this.idEstudiantes = idEstudiantes;
    }

    public Estudiantes(Integer idEstudiantes, String nombresE, String cursoE, String paraleloE, String correoE) {
        this.idEstudiantes = idEstudiantes;
        this.nombresE = nombresE;
        this.cursoE = cursoE;
        this.paraleloE = paraleloE;
        this.correoE = correoE;
    }

    public Integer getIdEstudiantes() {
        return idEstudiantes;
    }

    public void setIdEstudiantes(Integer idEstudiantes) {
        this.idEstudiantes = idEstudiantes;
    }

    public String getNombresE() {
        return nombresE;
    }

    public void setNombresE(String nombresE) {
        this.nombresE = nombresE;
    }

    public String getCursoE() {
        return cursoE;
    }

    public void setCursoE(String cursoE) {
        this.cursoE = cursoE;
    }

    public String getParaleloE() {
        return paraleloE;
    }

    public void setParaleloE(String paraleloE) {
        this.paraleloE = paraleloE;
    }

    public String getCorreoE() {
        return correoE;
    }

    public void setCorreoE(String correoE) {
        this.correoE = correoE;
    }

    @XmlTransient
    public Collection<Nota> getNotaCollection() {
        return notaCollection;
    }

    public void setNotaCollection(Collection<Nota> notaCollection) {
        this.notaCollection = notaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEstudiantes != null ? idEstudiantes.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estudiantes)) {
            return false;
        }
        Estudiantes other = (Estudiantes) object;
        if ((this.idEstudiantes == null && other.idEstudiantes != null) || (this.idEstudiantes != null && !this.idEstudiantes.equals(other.idEstudiantes))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Estudiantes[ idEstudiantes=" + idEstudiantes + " ]";
    }
    
}
